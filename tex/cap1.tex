\makeatletter
\def\input@path{{../}}
\makeatother

\documentclass[{../cosmo.tex}]{subfiles}
\begin{document}


\chapter{Bases observacionales de la cosmología}

La cosmología se entiende hoy en día como el origen, evolución y estructura del universo a gran escala. Para nosotros, lo más interesante ocurre para niveles más grandes que los de una galaxia. A modo de comparación, recordemos que una galaxia, típicamente, tiene los siguientes parámetros.

\begin{itemize}
    \item Contienen entre $10^7$ y $10^{3}$ estrellas
    \item Posee una masa de entre $ 10^{37}$ y $10^{44}$kg.
    \item Tiene un radio de 10 a 100pc.
\end{itemize}

Recordemos que un pc es un pársec y equivale a lo siguiente:

\begin{equation}
  1 \mathrm{pc} = 3.26 \mathrm{al} = 3.09 \cdot 10^{16}\mathrm{m}
\end{equation}

A estas escalas tan grandes, la gravitación es la única interacción. Se han observado también campos magnéticos galácticos, de entre $10^{-6}$G y de unos $10$kpc. También se han medido campos de $10^{-15}$G con longitudes de coherencia de $50$Mpc.


El objetivo de la cosmología es determinar la estructura del universo y de qué está compuesto, nuevamente a gran escala. Vamos a partir de los siguientes hechos observacionales:

\begin{itemize}
    \item El universo es estadísticamente homogéneo e isótropo a escalas superiores a los 100Mpc. Sí que presenta estructuras a escalas más pequeñas.
    \item El universo se expande de acuerdo con la ley de Hubble.
    \item Presenta un fondo de radiación térmico de microondas a $2.73$K. Este fondo es muy isótropo, aunque presenta fluctuaciones muy pequeñas, del orden de $10^{-5}$.
    \item El universo contiene materia bariónica, alrededor de un barión por cada $10^9$ fotones, aunque no hay cantidad apreciable de antimateria.
    \item La materia bariónica se compone de alrededor un 75\% de hidrógeno, un 25\% de helio. El resto de elementos sólo está presente en trazas.
    \item Los bariones constituyen un pequeño porcentaje de la densidad de energía total. El resto parece estar compuesto de materia oscura (25\%) con presión despreciable y de energía oscura(70\%).
\end{itemize}



\section{Galaxias}



Hay una gran diversidad de galaxias, tanto en tamaño y forma. Habitualmente, tienen entre $10$kpc y $100$kpc.

Desde los años 30, existe la secuencia de Hubble, que clasifica las galaxias según su forma.

\begin{figure}[H]
  \centering
  \includegraphics[scale=.5]{img/Hubble_sequence}
  \caption{Secuencia de Hubble}
\end{figure}

Las galaxias elípticas son aquellas que van dese E0 hasta S0. Son galaxias de forma lenticular, que tienen poco gas y poco polvo. Contienen estrellas antiguas, de población II.

Las galaxias espirales fueron las primeras en las que se observaron evidencias de materia oscura. Poseen un bulbo central que contienen estrellas antiguas, de población II, y en el centro un agujero negro supermasivo. En el caso de que las estrellas estén colapsando en el agujero negro, se habla entonces de núcleos galácticos activos.

A continuación hay un disco, que es aproximimadamente plano, con estrellas de población I, abundante gas y polvo. La curva de rotación de las estrellas sigue la ley de Tully-Fisher:

\begin{equation}
  L_B \propto v_{\mathrm{max}}^3
\end{equation}

La galaxia tiene también un halo, compuesto por estrellas de segunda población, que además tiene muchos cúmulos globulares y estrellas muy antiguas.

Analicemos un poco la curva de rotación de las galaxias espirales, es decir la velocidad de rotación en función de la distancia al centro. A la hora de analizar la curva de rotación (velocidad en función del radio) de los astros de la galaxia, se observó un movimiento anómalo:


\begin{figure}[H]
  \centering
  \includegraphics[scale=.5]{img/Galactic_rotation.png}
  \caption{Ejemplo de curva de rotación de una galaxia frente a una curva kepleriana.}
\end{figure}

Típicamente, la luminosidad de una galaxia espiral es la siguiente:

\begin{equation}
  L (r) = L_0 e^{ -\frac{r}{r_0}}
\end{equation}

La materia luminosa está cerca del disco, de tal manera que se podría considerar puntual y sus órbitas serían keplerianas tal que:

\begin{equation}
    v_{\mathrm{rot}} = \sqrt{ \frac{ G M_{ \mathrm{rot}} }{r} }
\end{equation}

Mediante esto, concluímos que la masa de la galaxia no tiene la distribución que esperábamos. Sin embargo, podemos invertir la ecuación y hallar la masa en función del radio, hallando que, para distancias alejadas del disco, tenemos:

\begin{equation}
    M_{ \mathrm{rot} } (r) = \frac{ v_m^2 r }{G}
\end{equation}

Dicho de otro modo, la masa crece linealmente con el radio. De este modo, la densidad de materia en función del radio es la siguiente:

\begin{equation}
    \rho (r) = \frac{ v^2_m }{ 4 \pi G r }
\end{equation}

En resumen, alrededor del disco se extiende mucha materia oscura. Si se saca la masa hallada mediante la rotación con la masa luminosa, se ve que es 10 veces mayor. La materia oscura parece tener mucha relevancia en las galaxias espirales.

Un ajuste más riguroso, usando el perfil de Navarro-Frank-White, arroja el siguiente resultado para la densidad:

\begin{equation}
    \rho (r) = \frac{ \rho_0 }{ \frac{r}{R_S} \left( 1 + \frac{r}{R_S} \right)^2 }
\end{equation}

En donde $R_S$ es el radio del disco.

Existe también otro perfil, denominado de Eninasto, que es el siguiente:

\begin{equation}
    \rho = \rho_0 e^{- \left( \frac{r}{R_S} \right)^\alpha }
\end{equation}

Parece ajustarse mejor a las galaxias enanas.



\section{Distribución de materia a gran escala}


Por encima de las galaxias, se forman también grupos de galaxias y cúmulos de galaxias. Ambos están ligados gravitacionalmente.

\begin{itemize}
  \item Grupo de galaxias: Contienen unas 100 galaxias, un radio de entre $100$kpc y $2$Mpc y una velocidad media de unos $200 ^{\mathrm{km}} / _{\mathrm{s}}$.
  \item Cúmulo de galaxias: Contienen entre 100 y 200 galaxias, tienen un radio de entre $2$Mpc y $10$Mpc y una velocidad media de $1000 ^{\mathrm{km}} / _{\mathrm{s}}$.
\end{itemize}

Al estar ligados gravitacionalmente, podemos usar el teorema del virial para hallar la masa virial de un cúmulo:

\begin{align}
  2 \ev{T} &= - \ev{V} \\
  \ev{T} &= \frac{1}{2} m \ev{v^2} \\
  \ev{V} &= -mGM \ev{\frac{1}{r}} \\
\end{align}

De este modo:

\begin{framed}
\begin{equation}
  M_{\mathrm{vir}} = \frac{ \ev{v^2} }{ G \ev{\frac{1}{r}} }
\end{equation}
\end{framed}

Ambos parámetros podemos hallarlos siempte, determinando la masa virial. Vamos a ver algunas estumaciones de grupos o cúmulos para ver qué papel tiene aquí la \textit{materia oscura}. Utilizaremos como masa luminosa $M_\mathrm{lum}$ lo siguiente:

\begin{equation}
  M_{\mathrm{lum}} = L \frac{ M_\odot }{ L_\odot }
\end{equation}

Luego de esto, lo que haremos será hacer coeficientes de masa virializada entre masa luminosa.

\begin{equation}
  \frac{ M_{\mathrm{vir} } }{ M_{\mathrm{lum}}} =%
  \frac{ M_{\mathrm{vir}} }{L} %
  \frac{ L_\odot }{ M_\odot } \equiv Y
\end{equation}

En donde $Y$ es el cociente masa-luminosidad. En el caso de que haya únicamente materia luminosa, se daría que $Y=1$. Sin embargo, haciendo este cálculo para el cúmulo de Virgo (en el que se encuentra la Vía Láctea), tenemos $Y=200$.

Este cálculo es similar para otros cúmulos. Esto quiere decir que también hay materia oscura, y que es la mayor parte de la masa de los cúmulos.


Por otro lado, están también los supercúmulos. Los supercúmulos contienen en torno a 100 grupos y cúmulos, y tienen un tamaño típico de entre $10$ y $70$Mpc. Este tipo de estructuras no están virializadas, lo cual quiere decir que aún están formándose.

A escala aún más grande, hay filamentos, vacíos y cúmulos. Los filamentos unen los muros, que se encuentrar aproximadamente a distancias de $100$Mpc. A unos $4300$Mpc hay una gran homogeneidad.

\todo{Completar tabla.}

\begin{figure}[H]
  \centering
  \begin{tabular}{lll}
    Tipo de estructura & Tamaño & Galaxias \\ \hline
    Grupo de galaxias & $100$kpc-$2$Mpc & $\approx 100$ galaxias \\
    Cúmulo de galaxias & $2$Mpc-$10$Mpc & $100$-$200$ galaxias \\
    Supercúmulos & $10$Mpc-$70$Mpc. & $100$ grupos y cúmulos \\
    Vacíos & & \\
    Filamentos & & \\
    Muros & & \\
  \end{tabular}
  \caption{}
\end{figure}



\section{Fluctuaciones de densidad en la materia}


Definimos la fluctuación de densidad media de la materia del siguiente modo:

\begin{equation}
  g (\vec{r}) =
\end{equation}

Calculado para el universo observable, tenemos el siguiente diagrama

\begin{figure}[H]
  \centering
  \includegraphics[scale=.4]{img/Density_fluctuations.png}
  \caption{Fluctuaciones de densidad media en esferas con distinto radio.}
\end{figure}

A escalar grandes (mayores a $100$Mpc), hay menos de un 10\% de fluctuaciones. A radios mayores de $100$Mpc, hay una grandísima homogeneidad estadística. De aquí, se enuncia el principio cosmológico.


\begin{framed}
  \textbf{Principio cosmológico}: Las propiedades del universo a gran escala son independientes de la posición y dirección del espacio.
\end{framed}

De aquí extraemos que el universo es homogéneo e isótropo. La isotropía surge al calcular la temperatura en el mismo diagrama, en cualquier punto. Al haber varios isotropía en varios puntos, hay homogeneidad. A esto se le suma lo siguiente:

\begin{framed}
  \textbf{Principio copernicano}: No ocupamos una posición privilegiada en el universo.
\end{framed}

Si hay isotropía en un punto y le añadimos el principio copernicano, tenemos también la homogeneidad.





\section{Densidad de materia del universo}


Hasta ahora hemos estimado la masa del universo a pequeña escala (menos de $10$Mpc) y vimos que había mayoritariamente algo que denominamos energía oscura. Si la matería a gran escala del universo fuera luminosa, podríamos hallar la masa de una región del espacio a partir de su luminosidad. Esto es lo que llamamos \textbf{densidad de luminosidad promedio} $\ev{\mathcal{L}}$. Aproximadamente, el valor de este parámetro es:

\begin{equation}
    \ev{\mathcal{L}} = ( 2.0 \pm 0.2 ) \cdot 10^8 h L_\odot \mathrm{Mpc}^{-3}
\end{equation}

Donde $h$ es el parámetro de Hubble reducido. De este modo, podríamos calcular la masa mediante la siguiente ecuación:

\begin{equation}
    \rho_m = \ev{\mathcal{L}} \frac{M}{L}
\end{equation}

En donde $M$ es la masa típica de una estrella. Sin embargo, esto no es una buena aproximación, ya que sabemos que no todas las estrellas tienen la misma masa. Sin embargo, esto nos sirve para estimar la contribución de distintos objetos astrofísicos a la densidad de masa.

Para el cálculo, vamos a partir de aquí. Generalmente, el cociente de esta ecuación se suele dar en fracciones de parámetros del Sol. La razón masa luminosidad $Y$ es la siguiente:

\begin{equation}
    Y = \frac{M}{L} \frac{M_\odot }{ M_\odot }
\end{equation}

De tal modo, que $Y_\odot = 1$.

Por otro lado, también se suele dar la densidad de materia local a partir de un valor de densidad crítico, con el siguiente valor:

\begin{equation}
    \rho_c = \frac{ 3\hubble_0^2 }{ 8 \pi G}= 1.88 \rhubble^2 \cdot 10^{-29} \text{g cm}^{-3}
\end{equation}

En donde $\hubble_0$ es el parámetro de Hubble a hoy en día y $G$ es la constante de la gravitación de Newton. Posteriormente veremos por qué se escoge este valor y no otro, pero de momento lo usaremos como parámetro de referencia.

Definimos también el parámetro $\Omega_m$ como el cociente de la densidad de materia entre la densidad crítica:

\begin{framed}
\begin{equation}
    \Omega_m = \frac{ \rho_m }{ \rho_c } = \frac{ \ev{\mathcal{L} } }{ \rho_c } \frac{ M_{\mathrm{sol}} }{ L_{\mathrm{sol}} } Y
\end{equation}
\end{framed}


Y el valor del parámetro densidad de materia luminosa tiene el siguiente valor:

\begin{framed}
  \begin{equation}
    \Omega_m = (1390)^{-1} h^{-1} Y
  \end{equation}
\end{framed}

Con esto ya podemos hacer algunos calculos interesantes



\paragraph{Materia luminosa}


En el caso de estrellas, el ratio $Y_*$ es aproximadamente 3. De este modo, el coeficiente de densidad de materia tendría la siguiente forma:

\begin{equation}
    \Omega_{M*} h = 0.002
\end{equation}

A lo largo del curso veremos que la densidad de materia real es próxima a la crítica, y sin embargo aquí vemos que la que hace referencia a las estrellas es bastante pequeña.

Si consideramos la materia luminosa de unas galaxias, que es idéntico a la de las estrellas, pero considerando nubes. En este caso:

\begin{equation}
    Y_{\mathrm{lum},\mathrm{galaxias}} = ( 5 - 10 ) h
\end{equation}

Con lo cual, la densidad de materia luminosa $\Omega_{\mathrm{lum}}$ estaría comprendida entre $0.0035$y $0.007$


Si queremos tener en cuenta la materia total de las galaxias a partir de sus curvas de rotación, obtenemos:

\begin{equation}
    Y_{\mathrm{tot} , \mathrm{galaxias}} \sim 43h
\end{equation}

Dándonos en total $\Omega_{\mathrm{tot} , \mathrm{galaxias}} \sim 0.03$.

En el caso de considerar la materia en cúmulos, obtenemos:

\begin{equation}
    Y_{\mathrm{tot} , \mathrm{galaxias}} = ( 213 \pm 53 ) h \quad \leadsto \quad %
    \Omega_{\mathrm{cumulos}} \sim 0.15 \pm 0.02 \pm 0.04(545)
\end{equation}

La densidad de materia de los cúmulos es una aproximación de la densidad de materia del universo, y de hecho, se cree que es una buena aproximación.

Por otro lado, si consideramos la densidad de materia de Planck, obtenemos un valor de $\Omega_M = 0.315 \pm 0.013$, en el que está incluída toda la materia del universo.


\paragraph{Materia bariónica}


Ahora vamos a considerar lo mismo, pero también para la materia bariónica. Para ello, vamos a ver cómo hallamos este valor. La formación de los elementos ligeros del universo únicamente ocurren entre el primer segundo de vida del universo y los tres primeros minutos de vida. En estas condiciones, se pueden formar D, $^3$He, $^4$He, $^7$Li. Los elementos más pesados se forman en supernovas.

La abundancia de los distintos elementos ligeros depende únicamente de la asimetría bariónica, que se define del siguiente modo:

\begin{equation}
    \eta = \frac{ n_B - n_{\overline{B}} }{n_\gamma} = ( 4.6 - 5.9 ) \cdot 10^{-10}
\end{equation}


Las medidas experimentales concuerdan bastante bien con las predichas mediante la teoría, por ello, las medidas de las abundancias nos permiten hallar la asimetría bariónica del universo. A partir de la temperatura del fondo cósmico de microondas, podemos hallar que en el universo hay unos $410 \mathrm{cm}^{-3}$. De aquí, podemos hallar el número de bariones del universo, que es $0.25 \mathrm{m}^{-3}$. A partir de aquí, podemos hallar la densidad bariónica del universo. Finalmente, a partir de aquí definimos el parámetro de densidad bariónico:

\begin{equation}
    \Omega_B = \frac{ \rho_B }{ \rho_c } = 0.0449 \pm 0.0028
\end{equation}

Llegados a este punto es interesante, es interesante comprobar los distintos parámetros de densidad. El problema de la materia oscura bariónica aparece porque no conseguimos detectarla, ya que no emite luz. Sin embargo, existe también el problema de la materia oscura no bariónica del universo, que contiene la mayor parte de la materia del universo:

\begin{equation}
    \Omega_M = 0.3 \gg \Omega_B = 0.04 \gg \Omega_{ \mathrm{lum} } = 0.005
\end{equation}

El candidato a la materia oscura no bariónica es algún tipo de partícula neutra, estable y con masa, siendo uno de los principales candidatos los neutrinos. Sin embargo, el problema es que son demasiado ligeros y que además no pueden formar estructuras pequeñas en el universo.


\section{Fondo cósmico de microondas}


El fondo cósmico de microondas es una radiación que emite como un cuerpo negro a $T_0 = 2.725 \pm 0.001$K. Puede calcularse el número de fotones:

\begin{equation}
    n_\gamma (T_0) = \frac{1}{\pi^2 } \int_0^\infty \frac{ \omega^2 \dd{\omega} }{ e^{ \frac{\omega}{T_0} } - 1 } = 410 \mathrm{cm} ^{-3}
\end{equation}

De lo cual, se halla que $\Omega_\gamma = 2.47 \cdot 10^{-5} h^{-2}$, con lo cual apenas contribuye a la masa del universo. Además de ello, es una radiación extremadamente isótropa.





\section{Abundancia de elementos ligeros}


% Clase del 28 de febrero de 2018


Todas estas medidas coinciden en el rango en el que se cumple la siguiente asimetría bariónica:

\begin{equation}
  \eta = \frac{ n_{\mathrm{B}} - n_{\overline{\mathrm{B}}}}{n_\gamma} = ( 4.6 - 5.9) \cdot 10^{-10}
\end{equation}

Con esta asimetría, también podemos calcular la densidad bariónica. El cálculo es el siguiente, suponiendo uqe los bariones sean no relativistas:

\begin{align}
  \rho_\baryon &= n_\baryon m_n = \eta n_\gamma m_n \\
  \Omega_\baryon h^2 &= \frac{\rho_\baryon h^2}{ \rho_c } = \frac{ \eta n_\gamma m_N h^2 }{ 1.88 h^2 10^{-29} \mathrm{g\ cm}^{-3} } = 0.022
\end{align}

Resultando finalmente en que la densidad bariónica actual es la siguiente:

\begin{framed}
  \begin{equation}
    \Omega_\baryon = 0.045
  \end{equation}
\end{framed}

Dicho de otro modo, es la abundancia actual de materia ordinaria. Podemos comparar esto con la densidad de materia en forma luminosa y total que calculamos anteriormente. Resulta que:

\begin{framed}
  \begin{align}
    \Omega_\mathrm{M} &\gg \Omega_\baryon &\gg \Omega_\mathrm{lum} \\
    0.3 &\gg 0.04 &\gg 0.005
  \end{align}
\end{framed}

Este es el llamado problema de la \textit{materia oscura bariónica}. Se cree que esta materia que falta está en forma de nubes de hidrógeno molecular en los halos de las galaxias.

Por otro lado, está también el problema de la \textit{materia oscura no bariónica}. La mayor parte de la materia no es materia ordinaria.

La materia oscura bariónica debe cumplir lo siguiente:

\begin{itemize}
  \item Son partículas con masa.
  \item Son estables.
  \item No tienen carga eléctrica o de color. Pueden ser sino débilmente interactuantes.
\end{itemize}

Intentando buscar una partícula del modelo estándar que cumpla esta característica, nos fulminamos todas, a excepción de los neutrinos $\nu_e, \nu_\mu, \nu_\tau$. Por otro lado, los neutrinos tienen cotas a sus masas, pero son muy pequeños. Podríamos ver cómo serían la distribución de las fluctuaciones de densidad de la materia suponiendo que la materia oscura sean neutrinos.

\missingfigure{Figura de fluctuación de densidad.}

Concretamente, se pueden formar estructuras hasta cúmulos o grupos, pero no a escalas más grandes. Por ello, no ello, en el modelo estandar no hay partículas candidatas a materia oscura fría (si son calientes, son relativistas entonces.)

Otro candidato de las extensiones del modelo estandar son las supersimetrías. Las supersimetrías son una simetría adicional que se le añade al modelo estándar, que consiste en que cada bosón tiene un fermión con las mismas propiedades y viceversa. El \textit{neutralino} es una partícula que es una composición del higgsino y del wino, que es candidato. Se ha buscado en el LHC, pero no se ha encontrado nada de supersimetrías, ni del neutralino, así que igual hay que descartarla.




\section{Fondo cósmico de microondas}


Hoy en día, la fuente más grande de información de cosmología que disponemos es el fondo cósmico de microondas.

Conforme vamos yendo hacia atrás observando el universo, vamos a llegar a un momento en el que todo lo que haya sea un plasma en equilibrio térmico con los fotones, materia... Dicho de otro modo:

\begin{equation}
  n_T (\omega) \dd{\omega} %
  = \frac{1}{\pi^2} \frac{ \omega^2 \dd{\omega} }{ e^{ \frac{\omega}{T}} - 1 }
\end{equation}

La radiación debería de ser como un cuerpo negro cuya temperatura es inversamente proporcional al factor de escala. Cuando el universo se hace eléctricamente neutro, los fotones dejan de interaccionar con la materia. Cuando se recombina la materia, el universo se hace transparente. Lo que observamos es cómo están los fotones en aquel momento.

Gamov calculó en 1940 que, cuando el universo a una temperatura de $0.1$MeV, unos 3 minutos después de la creación del universo, si aplicamos el factor de escala hoy en día esta radiación debe estar a unos $5$K.

Más adelante, en 1965, Penzias y Wilson, calibrando una antena de comunicación, encontraron un ruido en la señal que era isótropo, y comprobaron que el ruido tenía un máximo en torno a una longitud de onda de $7.35$cm, y estimaron que si era una radiación del cuerpo negro, tendría una temperatura de $3.5 \pm 1$K. Más adelante, Dicke, Peebles, Roll y Wilkinson identificaron este fondo cósmico de microondas con un remanente del Big-Bang.

En 1990, con el satélite COBE y el instrumento FIRAS, se localizó un pico en $0.2$cm, que sólo se puede detectar en el espacio debido a que lo absorbe la atmósfera de la Tierra. Encontraron que el CMB tenía una temperatura:

\begin{equation}
  T_0 = ( 2.725 \pm 0.001 )\mathrm{K}
\end{equation}

Ahora habría que integrar la expresión para hallar la densidad de fotones hoy:

\begin{equation}
  n_\gamma (T_0) = \frac{1}{\pi^2} \int_0 ^\infty \frac{\omega^2 \dd{\omega}}{ e^{ \frac{\omega}{T_0} } -1 } = 410 \mathrm{cm}^{-1}
\end{equation}

Con esto, ya podemos hallar la densidad de energía de fotones:

\begin{equation}
  \rho_\gamma (T_0) = \frac{\pi^2}{15} T_0^4
\end{equation}

Que se corresponde con el siguiente parámetro de densidad de fotones:

\begin{equation}
  \Omega_\gamma = \frac{\rho_\gamma}{\rho_c} = 2.47 \cdot 10^{-5} h^{-2}
\end{equation}

Se comprueba que hoy en día en el universo los fotones prácticamente no aportan nada de energía.

\begin{figure}[H]
  \centering
  \includegraphics{img/CMB_PW.jpg}
  \caption{Fondo cósmico de microondas que observaron Penzias y Wilson en 1965.}
\end{figure}


Puede verse facilmente lo isótropo que es el monopolo del CMB. Por otro lado, si queremos comprobar las diferencias de temperaturas en el CMB, podemos restar el monopolo. En este caso, obtenemos un dipolo.

\begin{figure}[H]
  \centering
  \includegraphics{img/CMB_COBE.jpg}
  \caption{Fondo cósmico de microondas de la sonda COBE.}
\end{figure}

Ahora vemos que hay una contribución dipolar. Hay fotones que tienen una temperatura mayor comparados con los que vienen de la otra parte de la galaxia. Concretamente, este dipolo tiene unas variaciones de temperatura $\delta T_1 = (3.372 \pm 0.007)$mK. Comparado con $T_0$:

\begin{equation}
  \frac{\delta T_1}{T_0} = 1.23 \cdot 10^{-3}
\end{equation}

La parte más caliente de este dipolo apunta justamente al cúmulo de Virgo, y puede describirse de la siguiente manera:

\begin{equation}
  T (\theta) = T_0 \left( 1 + \frac{v_0}{c} \cos \theta \right)
\end{equation}

Siendo entonces:

\begin{equation}
  \cos \theta = \vec{n} \cdot \vec{u}
\end{equation}

En donde $\vec{n}$ es la dirección de la observación y $\vec{u}$ es la dirección del movimiento. Por lo tanto, podemos interpretar que este dipolo es un efecto cinemático debido al movimiento del sistema solar con respecto al CMB. Así pues, $\frac{v_0}{c}$ es una medida de la velocidad con la que nos movemos con respecto al fondo de microondas, resultando en:

\begin{equation}
  v_0 = ( 371 \pm 0.5 ) \frac{\mathrm{km}}{\mathrm{s}}
\end{equation}


Sustrayendo el dipolo del CMB, entonces observamos lo siguiente:

\begin{figure}[H]
  \centering
  \includegraphics{img/CMB_COBE2.jpg}
  \caption{Fondo cósmico de microondas de la sonda COBE.}
\end{figure}

La parte más caliente que se ve en el centro es debido a la galaxia. En esta figura, la fluctuación más grande $\delta T_2 = 30 \pm 5 \mu$K, y comparándolo con la temperatura del CMB:

\begin{equation}
  \frac{\delta T_2}{T_0} = 1.1 \cdot 10^{-5}
\end{equation}

Esta imagen nos da una imagen de unos 300000 años después del Big-Bang.

Podemos concluir que en esta época el universo era muy homogéneo y no había formada ninguna estructura. Pero posteriormente, las sobredensidades del CMB es donde más adelante se van a construir las estructuras de gran escala del universo que podemos ver hoy en día.


En el año 2003, el satelite WMAP hizo las mismas medidas que hizo COBE, pero con una mayor resolución angular.

\begin{figure}[H]
  \centering
  \includegraphics{img/CMB_WMAP.jpg}
  \caption{Fondo cósmico de microondas de la sonda WMAP.}
\end{figure}

Esta imagen es la misma que se halló con el COBE, con la diferencia de que tiene mucha más resolución angular, y se han eliminado la contribución de la galaxia.

La mayor parte de las manchas tienen alrededor de un grado, siendo la resolución angular del WMAP menor a un grado. Este es un patrón característico. Los fotones que recibimos del CMB son de la LSS (last scaterring surface), situada a un redshift de aproximadamente $1100$. EStas manchas de un grado se corresponden a una distancia sobre la superficie de scaterring, que se corresponde con el horizonte de sonido. Se corresponden con el tiempo que una onda de sonido sobre el plasma pudo expandirse en el universo. Analizando estas manchas, podemos ver cómo era el universo en aquellas épocas.

\begin{figure}[H]
  \centering
  \includegraphics[scale=.9]{img/CMB_plank.jpg}
  \caption{Fondo cósmico de microondas de la sonda Plank.}
\end{figure}

Esta figura es del PLANCK. Las manchas tienen el mismo diámetro angular, pero mayor nitidez. A partir de este mapa puede hallarse el espectro de potencias de temperatura, y nos dice cómo se distribuyen las fluctuaciones en términos de los grados.

\begin{figure}[H]
  \centering
  \includegraphics[scale=.35]{img/CMB_fluctuations.png}
  \caption{Fluctuaciones de multipolos según los datos del fondo cósmico de microondas de la sonda Plank.}
\end{figure}


Se ve claramente que la curva está a un grado con máximos. Esta curva se puede generar con cálculos cosmológicos. Si comparamos la teoría con este gráfico, se ve que el modelo cosmológico (sombreado verde) ajusta muy bien con lo observado.

Con esta gráfica se pueden sacar todos los parámetros cosmológicos con errores menores al 1\% en muchos casos.




\section*{Valores de algunas constantes}

A lo largo de la asignatura, vamos a realizar el convenio $\hbar = c = k_\mathrm{B} = 1$.


\begin{figure}[H]
  \centering
  \begin{tabular}{cccc}
    Parámetro & etw  & Valor & Unidades \\ \hline
    Temperatura CMB & T & $2.725$ & K \\
    & & $ 2.348 \cdot 10^{-3}$ & eV \\
    Parámetro de Hubble & $H_0$ & $100 \rhubble$ & $\mathrm{km/s/Mpc}$  \\
    & & $ 2.133 \rhubble \cdot 10^{-33}$ & eV \\
    Tiempo de Hubble & $\hubble_0^{-1}$ & $ 9778 \rhubble^{-1} \cdot 10^9$ & años \\
    Radio de Hubble & $c \hubble_0^{-1}$ & $ 2997.9 \rhubble^{-1}$ & Mpc \\
    & & $ 2.348 \cdot 10^{-3}$ & eV \\
    Parámetro de Hubble reducido (CMB) & $\rhubble$ & $ 0.673$ &  \\
    (HST)& & $ 0.738$ & \\
  \end{tabular}
\end{figure}


Los parámetros de densidad que usaremos serán los siguientes:

\begin{figure}[H]
  \centering
  \begin{tabular}{ccccc}
    $\Omega_\mathrm{M}$ & $\Omega_\baryon$ & $\Omega_\Lambda$ & $\Omega_\gamma$ & $\eta$ \\ \hline
    $0.0315$ & $0.049$ & $0.685$ & $5.45 \cdot 10^{-5}$ & $6.02 \cdot 10^{10}$
  \end{tabular}
\end{figure}




\end{document}
